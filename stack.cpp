#include "stdio.h"
int maxSize =5;
int stack[5];
int top =-1;

int isFull(){
	if(top == maxSize){
		return 1;
	}else{
		return 0;
	}
}

int isEmpty(){
	if(top== -1){
		return 1;
	}else{
		return 0;
	}
}

void display(){
	for (int i = 0; i < maxSize; ++i)
		{
			printf("%d\n", stack[i]);
		}
}

int pop(){
	int item;
	if(isEmpty()){
		printf("stack is empty");
	}else{
		item = stack[top];
		top=top-1;
		return item;
	}
}

void push(int item){
	if (isFull())
	{
		printf("stack is full\n");
	}else{
		top=top+1;
		stack[top] = item;
	}
}

int main(){
	push(1);
	push(2);
	push(3);
	push(9);
	push(7);
	printf("before pop" );
	display();
	printf("pop: %d\n",pop() );
	printf("pop: %d\n",pop() );
	printf("pop: %d\n",pop() );
	printf("pop: %d\n",pop() );
	printf("pop: %d\n",pop() );
	printf("pop: %d\n",pop() );
	printf("after pop" );
	return 0;

}